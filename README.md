LightSpeed Voice Todo
=====================

Installation & Configuration
----------------------------

Clone the repository
```bash
git clone git@gitlab.com:judahnator/lsv_interview.git
```

Enter the directory you cloned the project into, then you will need to install composer dependencies.
```bash
composer install
```

Set up your env, copy the `.env.example` file to `.env`. Change any settings you like, if you are using a SQLITE backend then comment out the DB_DATABASE attribute and set the `DB_CONNECTION` to "sqlite".

Finally, you should be able to run the internal server with:

```bash
php artisan serve
```

Usage
-----

With the web server running, visit http://127.0.0.1:8000/ in your browser. You will be prompted to log in. If you do not already hav ean account, click "register". When you register an account some sample todo items are created for you.

Once logged in, the home page will list your todo items. You may edit or delete them, as well as add new items with the form at the bottom.

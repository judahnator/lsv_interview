@php
    use App\Models\ToDo;
    /** @var ToDo $todo */
@endphp

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Editing Todo</div>

                    <div class="card-body">
                        <form action="{{ route('todos.update', $todo) }}" method="post">
                            @csrf
                            @method('patch')
                            <input type="text" class="form-control" name="contents" value="{{ $todo->getAttribute('contents') }}">
                            <input type="date" class="form-control" name="due date" value="{{ $todo->getAttribute('due_date')?->format('Y-m-d') }}">
                            <select name="tagged[]" multiple class="custom-select">
                                @foreach(\App\Models\User::all(['id', 'name']) as $user)
                                <option
                                    value="{{ $user->getKey() }}"
                                    @if($todo->tagged->pluck('id')->contains($user->getKey())) selected @endif
                                >
                                    {{ $user->getAttribute('name') }}
                                </option>
                                @endforeach
                            </select>
                            <input type="submit" class="form-control btn btn-primary" value="Make it so!">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

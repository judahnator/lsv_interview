@php
    use App\Models\ToDo;
    use Illuminate\Database\Eloquent\Collection;
    /** @var Collection<ToDo> $todos */
@endphp

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <ul class="list-group">
                        @foreach($todos as $todo)
                        <li class="list-group-item">
                            <a class="btn btn-info" href="{{ route('todos.edit', $todo) }}">Update</a>
                            <a class="btn btn-danger" onclick="destroyTodo({{ $todo->getKey() }})">Delete</a>
                            |
                            {{ $todo->getAttribute('contents') }} @if($todo->getAttribute('due_date')) - Due in {{ now()->diffForHumans($todo->getAttribute('due_date'), \Carbon\CarbonInterface::DIFF_ABSOLUTE) }} @endif
                            @if ($todo->tagged->isNotEmpty()) | ({{ $todo->tagged->pluck('name')->implode(', ') }})@endif
                        </li>
                        @endforeach
                        <li class="list-group-item">
                            <form method="post" action="{{ route('todos.store') }}">
                                @csrf
                                <input type="text" class="form-control" name="contents" placeholder="My New Todo!">
                                <input type="date" class="form-control" name="due_date" min="{{ now()->format('Y-m-d') }}" placeholder="Due Date">
                                <select name="tagged[]" multiple class="custom-select">
                                    @foreach(\App\Models\User::all(['id', 'name']) as $user)
                                        <option value="{{ $user->getKey() }}">{{ $user->getAttribute('name') }}</option>
                                    @endforeach
                                </select>
                                <input type="submit" class="btn btn-primary btn-block" value="Make it so!">
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<form id="destroy-form" method="POST" class="d-none">
    @csrf
    <input type="hidden" name="_method" value="DELETE">
</form>

<script>
    function destroyTodo(action) {
        let form = document.getElementById('destroy-form');
        form.setAttribute('action', 'todos/' + action);
        form.submit();
    }
</script>
@endsection

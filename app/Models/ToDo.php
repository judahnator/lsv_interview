<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations as Relationship;

/**
 * @property Collection $tagged
 * @property User $user
 */
final class ToDo extends Model
{
    use HasFactory;

    protected $dates = [
        'due_date',
    ];

    protected $fillable = [
        'user_id',
        'contents',
        'due_date',
    ];

    protected $table = 'todos';

    public function tagged(): Relationship\BelongsToMany
    {
        return $this->belongsToMany(User::class, 'todos_users', 'todo_id');
    }

    public function user(): Relationship\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}

<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\ToDoRequest;
use App\Models\ToDo;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

final class ToDoController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param ToDoRequest $request
     * @return RedirectResponse
     */
    public function store(ToDoRequest $request)
    {
        /** @var ToDo $todo */
        $todo = Auth::user()->todos()->create($request->only('contents', 'due_date'));
        $todo->tagged()->sync($request->get('tagged'));
        return back()->with('status', 'Todo added!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ToDo $todo
     * @return Response
     */
    public function edit(ToDo $todo)
    {
        return response()->view('todo.edit', ['todo' => $todo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ToDoRequest $request
     * @param ToDo $todo
     * @return RedirectResponse
     */
    public function update(ToDoRequest $request, ToDo $todo)
    {
        static::verifyOwnership($todo, $request->user());
        $todo->update($request->only('contents', 'due_date'));
        $todo->tagged()->sync($request->get('tagged'));
        return response()->redirectTo(RouteServiceProvider::HOME)->with('status', 'Todo updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ToDo $todo
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(ToDo $todo)
    {
        self::verifyOwnership($todo, Auth::user());
        $todo->deleteOrFail();
        return back()->with('status', 'Item deleted!');
    }

    /**
     * @throws AuthorizationException
     */
    private static function verifyOwnership(ToDo $todo, User $owner): void
    {
        if ($todo->user()->isNot($owner)) {
            throw new AuthorizationException('You are not allowed to perform these actions on other users items.');
        }
    }
}

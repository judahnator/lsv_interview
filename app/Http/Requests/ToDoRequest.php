<?php declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

final class ToDoRequest extends Request
{
    public function authorize(): bool
    {
        return Auth::check();
    }

    #[ArrayShape([
        'contents' => "string",
        'due_date' => "string",
        'tagged'   => "string",
        'tagged.*' => "string",
    ])]
    public function rules(): array
    {
        return [
            'contents' => 'required|string',
            'due_date' => 'nullable|date',
            'tagged' => 'array',
            'tagged.*' => 'int|exists:users,id',
        ];
    }
}

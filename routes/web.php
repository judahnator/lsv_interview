<?php

use App\Http\Controllers as Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::redirect('/', RouteServiceProvider::HOME);

Route::get('/home', [Controller\HomeController::class, 'index'])->name('home');

Route::resource('todos', Controller\ToDoController::class,)->except('create', 'index', 'show');
